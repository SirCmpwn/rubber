package config

import (
	"code.google.com/p/gcfg"
)

type Config struct {
	Listen struct {
		Port int
	}
	Network struct {
		Address string
		Name string
		Nick string
		User string
		Password string
		RealName string
	}
}

func ReadConfig(file string, config *Config) error {
	return gcfg.ReadFileInto(config, "config.ini")
}
